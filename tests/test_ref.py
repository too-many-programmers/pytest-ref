# -*- coding: utf-8 -*-

import os
import tempfile

import pytest

from pytest_plugin_helpers.tester import isolate


@isolate
def test_fixture_ref_default(ref, ref_path):
    assert ref.get(default="yolo") == "yolo"
    assert ref.get() is None


@isolate(cmd_args="--create")
def test_fixture_ref_get_with_create(ref, ref_path):
    assert ref.get(default="yolo") == "yolo"
    assert ref.get() == "yolo"

    fullpath = os.path.join(ref_path, ref.test_key())
    assert os.path.exists(fullpath)

    with open(fullpath) as f:
        assert f.read() == "yolo"


@isolate(cmd_args="--create")
def test_fixture_ref_compare_with_create(ref, ref_path):
    assert ref.compare("yolo") is True
    assert ref.compare("yolo") is True

    fullpath = os.path.join(ref_path, ref.test_key())
    assert os.path.exists(fullpath)

    with open(fullpath) as f:
        assert f.read() == "yolo"


def test_default_ref_path(ref_path):
    assert ref_path == os.path.join(tempfile.gettempdir(), "pytest_ref")


@pytest.fixture(
    params=["something", "another",]
)
def anything(request):
    return request.param


def test_ref_fixture_auto_naming_get(ref):
    assert ref.test_key() == "test_ref__test_ref_fixture_auto_naming_get"

    #  No reference found, default value is returned
    assert ref.get() is None
    assert ref.get(key=None) is None
    assert ref.get(key=None, default=None) is None
    assert ref.get(key=None, default="yolo") == "yolo"
    assert ref.get(default="yolo") == "yolo"

    assert ref.compare("yolo") is False
    assert ref.compare("yolo", key=None) is False


def test_ref_fixture_auto_naming(ref, anything):
    assert ref.test_key() == "test_ref__test_ref_fixture_auto_naming[{}]".format(
        anything
    )


def test_ref_fixture_get(ref):
    assert ref.get(key="allo") is None
    assert ref.get(key="allo", default="yolo") == "yolo"


def test_ref_fixture_compare(ref):
    assert ref.compare("yolo") is False
    assert ref.compare("yolo", key="allo") is False
